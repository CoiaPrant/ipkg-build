package tar_gz

import (
	"archive/tar"
	"compress/gzip"
	"io"
)

type TarGZipReader struct {
	*tar.Reader
	gz *gzip.Reader
}

func NewTarGZipReader(r io.Reader) (*TarGZipReader, error) {
	gz, err := gzip.NewReader(r)
	if err != nil {
		return nil, err
	}

	tgz := tar.NewReader(gz)
	return &TarGZipReader{
		Reader: tgz,
		gz:     gz,
	}, nil
}

func (r *TarGZipReader) Close() error {
	return r.gz.Close()
}

type TarGZipWriter struct {
	*tar.Writer
	gz *gzip.Writer
}

func NewTarGZipWriter(w io.Writer) *TarGZipWriter {
	gz := gzip.NewWriter(w)
	tgz := tar.NewWriter(gz)
	return &TarGZipWriter{
		Writer: tgz,
		gz:     gz,
	}
}

func (w *TarGZipWriter) Close() error {
	defer w.gz.Close()
	return w.Writer.Close()
}

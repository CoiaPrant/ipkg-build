package po2lmo

import (
	"bytes"
	"encoding/binary"
	"sort"

	"github.com/robfig/gettext-go/gettext/po"
)

type Compiler struct {
	src *po.File
}

func NewCompiler(src *po.File) *Compiler {
	return &Compiler{
		src: src,
	}
}

func (c *Compiler) CompileLMO() []byte {
	b := bytes.NewBuffer(nil)

	entries := make([]LmoEntry, 0, len(c.src.Messages))

	for _, msg := range c.src.Messages {
		keyId := sfhHash(msg.MsgId)
		valueId := sfhHash(msg.MsgStr)

		if keyId == valueId {
			continue
		}

		length := len(msg.MsgStr)
		entries = append(entries, LmoEntry{
			KeyId:   keyId,
			ValueId: valueId,
			Offset:  uint32(b.Len()),
			Length:  uint32(length),
		})
		b.WriteString(msg.MsgStr)

		padding := (4 - length%4) % 4
		for i := 0; i < padding; i++ {
			b.WriteByte(0)
		}
	}

	beginOffset := uint32(b.Len())

	sort.Slice(entries, func(i, j int) bool {
		return entries[i].KeyId < entries[j].KeyId
	})
	for _, entry := range entries {
		binary.Write(b, binary.BigEndian, entry)
	}

	binary.Write(b, binary.BigEndian, beginOffset)
	return b.Bytes()
}

package po2lmo

type LmoEntry struct {
	KeyId   uint32
	ValueId uint32
	Offset  uint32
	Length  uint32
}

func sfhGet16(data string) uint32 {
	return (uint32(data[1]) << 8) + uint32(data[0])
}

func sfhHash(data string) uint32 {
	hash := uint32(len(data))
	rem := len(data) & 3
	len := len(data) >> 2

	if hash <= 0 {
		return 0
	}

	for ; len > 0; len-- {
		hash += sfhGet16(data)
		tmp := (sfhGet16(data[2:]) << 11) ^ hash
		hash = (hash << 16) ^ tmp
		data = data[4:]
		hash += hash >> 11
	}

	switch rem {
	case 3:
		hash += sfhGet16(data)
		hash ^= hash << 16
		hash ^= uint32(data[2]) << 18
		hash += hash >> 11
	case 2:
		hash += sfhGet16(data)
		hash ^= hash << 11
		hash += hash >> 17
	case 1:
		hash += uint32(data[0])
		hash ^= hash << 10
		hash += hash >> 1
	}

	hash ^= hash << 3
	hash += hash >> 5
	hash ^= hash << 4
	hash += hash >> 17
	hash ^= hash << 25
	hash += hash >> 6

	return hash
}

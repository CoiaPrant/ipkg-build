package translation

var languages = map[string]string{
	"pt_br": "Português do Brasil (Brazilian Portuguese)",
	"ar":    "العربية (Arabic)",
	"bg":    "български (Bulgarian)",
	"bn":    "বাংলা (Bengali)",
	"ca":    "Català (Catalan)",
	"cs":    "Čeština (Czech)",
	"da":    "Dansk (Danish)",
	"de":    "Deutsch (German)",
	"el":    "Ελληνικά (Greek)",
	"en":    "English",
	"es":    "Español (Spanish)",
	"fi":    "Suomi (Finnish)",
	"fr":    "Français (French)",
	"he":    "עִבְרִית (Hebrew)",
	"hi":    "हिंदी (Hindi)",
	"hu":    "Magyar (Hungarian)",
	"it":    "Italiano (Italian)",
	"ja":    "日本語 (Japanese)",
	"ko":    "한국어 (Korean)",
	"mr":    "Marāṭhī (Marathi)",
	"ms":    "Bahasa Melayu (Malay)",
	"no":    "Norsk (Norwegian)",
	"pl":    "Polski (Polish)",
	"pt":    "Português (Portuguese)",
	"ro":    "Română (Romanian)",
	"ru":    "Русский (Russian)",
	"sk":    "Slovenčina (Slovak)",
	"sv":    "Svenska (Swedish)",
	"tr":    "Türkçe (Turkish)",
	"uk":    "Українська (Ukrainian)",
	"vi":    "Tiếng Việt (Vietnamese)",
	"zh_cn": "简体中文 (Chinese Simplified)",
	"zh_tw": "繁體中文 (Chinese Traditional)",
}

func Get(language string) string {
	if desc, ok := languages[language]; ok {
		return desc
	}

	return language
}

# IPKG-Build

IPKG-Build is a rewrite of the OpenWRT Software `ipk` build tool in `Go`

> IPKG-Build 是用 `Go` 语言重写的 OpenWRT Software `ipk` 构建工具

# Install

> 安装

- If Go toolchain was installed on system

  > 如果系统安装了 Go 工具链

  ```
  go install gitlab.com/CoiaPrant/ipkg-build/cmd/ipkg-build@latest
  go install gitlab.com/CoiaPrant/ipkg-build/cmd/ipkg-make-index@latest
  ```

- Or download release and put it in `$PATH`

  > 或者 从发布下载并放置到 `$PATH` 的目录下

  - For Windows

    `$PATH` is `C:/Windows/System32/`

  - For Linux

    `$PATH` is `/usr/bin`

    Go to [Download Release](https://gitlab.com/CoiaPrant/ipkg-build/-/releases)

# Compatibility

> 兼容性

The directory structure is the same as that of the OpenWRT official builder

> 目录结构与 OpenWRT 官方构建器的一样

But there is a little difference

The official OpenWRT builder gets control information from the `Makefile`, while `ipkg-build` gets it from `meta.json` and `control` dir

> 但是有一点不同

> OpenWRT 官方构建器从`Makefile`获取控制信息, 而 `ipkg-build` 从 `meta.json` 和 `control` 目录 获取

Example in `luci-app-example`

> 例子在 `luci-app-example`

## ⚠️ Note for packing on Microsoft Windows

> ⚠️ 在 `Microsoft Windows` 打包的注意事项

`Microsoft Windows` does not have the same permission bits as `Linux` to control whether files have executable permissions. When packaging on `Microsoft Windows`, some files will lose executable permissions!

> `Microsoft Windows` 并没有像 `Linux` 一样的权限位来控制文件是否具有可执行权限, 当在 `Microsoft Windows` 上打包时部分文件会丢失可执行权限

But you can override permissions on certain files using `perms.json` (this file is optional)

> 但是您可以使用 `perms.json` 覆盖某些文件的权限 (此文件是可选的)

# Usage

> 用法

```
root@localhost ~ # cd luci-app-example # Enter project dir (进入项目目录)
root@localhost ~/luci-app-example # ipkg-build --arch all --version "git_xxxxx_xxxx" # Pack ipk (打包 ipk)
```

Packed `ipk` will be output to `dist`

> 打包好的 `ipk` 将输出到 `dist`

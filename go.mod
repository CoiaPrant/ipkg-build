module gitlab.com/CoiaPrant/ipkg-build

go 1.21

require (
	github.com/robfig/gettext-go v0.0.0-20141023015941-548cda1a137a
	gitlab.com/CoiaPrant/clog v0.0.0-20240125121733-757221e3362e
	golang.org/x/text v0.14.0
)

require (
	github.com/fatih/color v1.16.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.18.0 // indirect
)

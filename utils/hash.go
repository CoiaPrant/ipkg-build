package utils

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
)

func MD5[T ~string | ~[]byte](data T) string {
	return hex.EncodeToString(Md5(data))
}

func Md5[T ~string | ~[]byte](data T) []byte {
	h := md5.New()
	h.Write([]byte(data))
	return h.Sum(nil)
}

func SHA256[T ~string | ~[]byte](data T) string {
	return hex.EncodeToString(Sha256(data))
}

func Sha256[T ~string | ~[]byte](data T) []byte {
	h := sha256.New()
	h.Write([]byte(data))
	return h.Sum(nil)
}

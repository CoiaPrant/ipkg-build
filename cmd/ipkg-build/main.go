package main

import (
	"archive/tar"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/robfig/gettext-go/gettext/po"
	"gitlab.com/CoiaPrant/clog"
	"gitlab.com/CoiaPrant/ipkg-build/pkg/po2lmo"
	"gitlab.com/CoiaPrant/ipkg-build/pkg/tar_gz"
	"gitlab.com/CoiaPrant/ipkg-build/pkg/translation"
	"golang.org/x/text/language"
)

const (
	Perm     = 0644
	ExecPerm = 0755

	i18nPath     = "po"
	debianBinary = "2.0\n"
)

var (
	priotitys = map[string]int{
		"Package":         1,
		"Version":         2,
		"Depends":         3,
		"Source":          4,
		"SourceName":      5,
		"License":         6,
		"Section":         7,
		"SourceDateEpoch": 8,
		"Architecture":    9,
		"Installed-Size":  10,
		"Description":     103,
	}
)

type Perms struct {
	Files   map[string]int64
	Default int64
}

type Package struct {
	PackageName  string
	Version      string
	Architecture string
	Meta         map[string]string
}

func main() {
	pkg := &Package{}

	{
		flag.StringVar(&pkg.Architecture, "arch", "all", "The pkg required arch.")
		flag.StringVar(&pkg.Version, "version", "dev", "The pkg builtin version.")
		flag.BoolVar(clog.DebugFlag(), "debug", false, "Enable verbose log")
		help := flag.Bool("h", false, "Show help")
		flag.Parse()

		if *help {
			flag.PrintDefaults()
			return
		}
	}

	{
		pwd, err := os.Getwd()
		if err != nil {
			clog.Fatalf("[Env] Failed to get working dir, error: %s", err)
			return
		}
		pwd = strings.ReplaceAll(pwd, "\\", "/")

		pkg.PackageName = path.Base(pwd)
	}

	clog.Info("Loading package meta...")
	pkg.Meta = make(map[string]string)
	{
		metadata, err := os.ReadFile("meta.json")
		if err != nil {
			clog.Fatalf("Failed to read meta data, error: %s", err)
			return
		}

		err = json.Unmarshal(metadata, &pkg.Meta)
		if err != nil {
			clog.Fatalf("Failed to unmarshal meta data, error: %s", err)
			return
		}
	}

	// Package
	pkg.packMain()

	// i18n
	{
		info, err := os.Stat("po")
		if err != nil {
			return
		}

		if !info.IsDir() {
			return
		}

		filepath.Walk(i18nPath, func(filePath string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info.IsDir() {
				return nil
			}

			filePath = strings.ReplaceAll(filePath, "\\", "/")
			if !strings.HasSuffix(filePath, ".po") {
				return nil
			}

			relativePath, err := filepath.Rel(i18nPath, filePath)
			if err != nil {
				return err
			}
			relativePath = strings.ReplaceAll(relativePath, "\\", "/")

			languageCode := path.Dir(relativePath)
			if languageCode == "en" {
				clog.Infof("[i18n] Skipped %s", languageCode)
				return nil
			}

			language, err := language.Parse(languageCode)
			if err != nil {
				clog.Errorf("[i18n] Skipped %s because unable to parse language code, error: %s", languageCode, err)
				return nil
			}

			base, _ := language.Base()
			region, _ := language.Region()
			if language.Parent().IsRoot() {
				languageCode = base.String()
			} else {
				languageCode = fmt.Sprintf("%s-%s", base, strings.ToLower(region.String()))
			}

			clog.Infof("[i18n] Loading translation %s source %s", languageCode, filePath)
			src, err := po.Load(filePath)
			if err != nil {
				clog.Errorf("[i18n] Skipped %s because unable to load translation source, error: %s", languageCode, err)
				return nil
			}

			clog.Infof("[i18n] Packing language pack for %s", languageCode)

			pkg.packI18n(languageCode, info, po2lmo.NewCompiler(src).CompileLMO())
			return nil
		})
	}
}

func (pkg *Package) packMain() {
	meta := metaClone(pkg.Meta)

	// Setup meta
	clog.Info("Setting package meta...")
	{
		meta["Package"] = pkg.PackageName
		meta["Version"] = pkg.Version
		meta["Architecture"] = pkg.Architecture
	}

	clog.Info("Loading package perms...")
	var perms Perms
	func() {
		data, err := os.ReadFile("perms.json")
		if err != nil {
			clog.Debugf("Skipped read perms data, error: %s", err)
			return
		}

		err = json.Unmarshal(data, &perms.Files)
		if err != nil {
			clog.Debugf("Skipped unmarshal perms data, error: %s", err)
			return
		}

		for k, perm := range perms.Files {
			perms.Files[k] = convertPermBits(perm)
		}
	}()

	clog.Info("Packing data...")
	data := bytes.NewBuffer(nil)
	{
		data := tar_gz.NewTarGZipWriter(data)
		addFiles(data, "www", "htdocs", perms)
		addFiles(data, "", "root", perms)
		data.Close()
	}
	meta["Installed-Size"] = fmt.Sprint(data.Len())

	{
		metadata := bytes.NewBuffer(nil)
		for k, v := range meta {
			metadata.WriteString(fmt.Sprintf("%s: %s\n", k, v))
		}

		os.Mkdir("control", ExecPerm)

		os.WriteFile("control/control", metadata.Bytes(), Perm)
		defer os.Remove("control/control")

		clog.Debugf("Output temporary meta to control/control")
	}

	clog.Info("Packing control...")
	control := bytes.NewBuffer(nil)
	{
		var perms Perms
		perms.Files = make(map[string]int64)
		perms.Default = ExecPerm

		perms.Files["control"] = Perm

		control := tar_gz.NewTarGZipWriter(control)
		addFiles(control, "", "control", perms)
		control.Close()
	}

	clog.Info("Packing package...")
	ipk := bytes.NewBuffer(nil)
	{
		ipk := tar_gz.NewTarGZipWriter(ipk)

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeDir,
			Name:     "./",
			Mode:     ExecPerm,
			ModTime:  time.Now(),
		})

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./control.tar.gz",
			Mode:     Perm,
			Size:     int64(control.Len()),
			ModTime:  time.Now(),
		})
		ipk.Write(control.Bytes())

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./data.tar.gz",
			Mode:     Perm,
			Size:     int64(data.Len()),
			ModTime:  time.Now(),
		})
		ipk.Write(data.Bytes())

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./debian-binary",
			Mode:     Perm,
			Size:     int64(len(debianBinary)),
			ModTime:  time.Now(),
		})
		ipk.Write([]byte(debianBinary))

		ipk.Close()
	}

	os.MkdirAll(fmt.Sprintf("dist/%s", pkg.Architecture), ExecPerm)

	outputPath := fmt.Sprintf("dist/%s/%s_%s_%s.ipk", pkg.Architecture, pkg.PackageName, pkg.Version, pkg.Architecture)
	os.WriteFile(outputPath, ipk.Bytes(), Perm)

	clog.Successf("Output package to %s", outputPath)
}

func (pkg *Package) packI18n(language string, info fs.FileInfo, payload []byte) {
	meta := metaClone(pkg.Meta)

	// Setup meta
	clog.Infof("Setting i18n (%s) meta...", language)
	{
		meta["Package"] = fmt.Sprintf("%s-%s", strings.ReplaceAll(pkg.PackageName, "luci-app-", "luci-i18n-"), language)
		meta["Version"] = pkg.Version
		meta["Architecture"] = "all"
		meta["Depends"] = strings.Join([]string{"libc", pkg.PackageName}, ", ")
		meta["Description"] = fmt.Sprintf("Translation for %s - %s", pkg.PackageName, translation.Get(language))
	}

	clog.Infof("Packing i18n (%s) data...", language)
	data := bytes.NewBuffer(nil)
	{
		data := tar_gz.NewTarGZipWriter(data)
		data.WriteHeader(&tar.Header{
			Typeflag: tar.TypeDir,
			Name:     "./",
			Mode:     ExecPerm,
			ModTime:  info.ModTime(),
		})

		name := strings.TrimSuffix(info.Name(), ".po")
		filepath := fmt.Sprintf("usr/lib/lua/luci/i18n/%s.%s.lmo", name, language)
		clog.Debugf("Adding translation (%s) %s.po to %s", language, name, filepath)

		data.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./" + filepath,
			Mode:     Perm,
			Size:     int64(len(payload)),
			ModTime:  info.ModTime(),
		})
		data.Write(payload)
		data.Close()
	}
	meta["Installed-Size"] = fmt.Sprint(data.Len())

	{
		keys := make([]string, 0, len(meta))
		for k := range meta {
			keys = append(keys, k)
		}

		sort.Slice(keys, func(i, j int) bool {
			priotity_i, priotity_j := priotitys[keys[i]], priotitys[keys[j]]
			if priotity_i == 0 {
				priotity_i = 50
			}

			if priotity_j == 0 {
				priotity_j = 50
			}

			return priotity_i < priotity_j
		})

		metadata := bytes.NewBuffer(nil)
		for _, k := range keys {
			metadata.WriteString(fmt.Sprintf("%s: %s\n", k, meta[k]))
		}

		os.Mkdir("control", ExecPerm)

		os.WriteFile("control/control", metadata.Bytes(), Perm)
		defer os.Remove("control/control")

		clog.Debugf("Output temporary i18n %s meta to control/control", language)
	}

	clog.Infof("Packing i18n (%s) control...", language)
	control := bytes.NewBuffer(nil)
	{
		var perms Perms
		perms.Files = make(map[string]int64)
		perms.Default = ExecPerm

		perms.Files["control"] = Perm

		control := tar_gz.NewTarGZipWriter(control)
		addFiles(control, "", "control", perms)
		control.Close()
	}

	clog.Infof("Packing i18n %s...", language)
	ipk := bytes.NewBuffer(nil)
	{
		ipk := tar_gz.NewTarGZipWriter(ipk)

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeDir,
			Name:     "./",
			Mode:     ExecPerm,
			ModTime:  time.Now(),
		})

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./control.tar.gz",
			Mode:     Perm,
			Size:     int64(control.Len()),
			ModTime:  time.Now(),
		})
		ipk.Write(control.Bytes())

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./data.tar.gz",
			Mode:     Perm,
			Size:     int64(data.Len()),
			ModTime:  time.Now(),
		})
		ipk.Write(data.Bytes())

		ipk.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     "./debian-binary",
			Mode:     Perm,
			Size:     int64(len(debianBinary)),
			ModTime:  time.Now(),
		})
		ipk.Write([]byte(debianBinary))

		ipk.Close()
	}

	os.MkdirAll("dist/all", ExecPerm)

	outputPath := fmt.Sprintf("dist/all/%s-%s_%s_all.ipk", strings.ReplaceAll(pkg.PackageName, "luci-app-", "luci-i18n-"), language, pkg.Version)
	os.WriteFile(outputPath, ipk.Bytes(), Perm)

	clog.Successf("Output i18n %s package to %s", language, outputPath)
}

func metaClone(mt map[string]string) map[string]string {
	meta := make(map[string]string)
	for k, v := range mt {
		meta[k] = v
	}
	return mt
}

func addFiles(w *tar_gz.TarGZipWriter, prefix, dir string, perms Perms) error {
	dir = strings.ReplaceAll(dir, "\\", "/")

	return filepath.Walk(dir, func(filePath string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		filePath = strings.ReplaceAll(filePath, "\\", "/")

		relativePath, err := filepath.Rel(dir, filePath)
		if err != nil {
			return err
		}

		if relativePath == "" || relativePath == "." || relativePath == ".." {
			return nil
		}

		relativePath = path.Join(prefix, relativePath)
		relativePath = strings.ReplaceAll(relativePath, "\\", "/")

		clog.Debugf("Adding %s to %s", filePath, relativePath)

		header, err := tar.FileInfoHeader(info, info.Name())
		if err != nil {
			return err
		}
		header.Name = "./" + relativePath

		if perm, ok := perms.Files[relativePath]; ok {
			header.Mode = perm
		} else if perms.Default != 0 {
			header.Mode = perms.Default
		}

		err = w.WriteHeader(header)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(filePath)
		if err != nil {
			return err
		}
		defer file.Close()

		_, err = io.Copy(w, file)
		return err
	})
}

func convertPermBits(perm int64) int64 {
	perm, _ = strconv.ParseInt(strconv.FormatInt(perm, 10), 8, 64)
	return perm
}

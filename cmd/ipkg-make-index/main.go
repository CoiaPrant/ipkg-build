package main

import (
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"

	"gitlab.com/CoiaPrant/clog"
	"gitlab.com/CoiaPrant/ipkg-build/pkg/tar_gz"
	"gitlab.com/CoiaPrant/ipkg-build/utils"
)

const (
	Perm     = 0644
	ExecPerm = 0755
)

var (
	packages []Package

	priotitys = map[string]int{
		"Package":        1,
		"Version":        2,
		"Depends":        3,
		"License":        4,
		"Section":        5,
		"Architecture":   6,
		"Installed-Size": 7,
		"Filename":       100,
		"Size":           101,
		"SHA256sum":      102,
		"Description":    103,
	}
)

type Package map[string]string

func main() {
	{
		flag.BoolVar(clog.DebugFlag(), "debug", false, "Enable verbose log")
		help := flag.Bool("h", false, "Show help")
		flag.Parse()

		if *help {
			flag.PrintDefaults()
			return
		}
	}

	clog.Infof("Loading packages...")
	err := filepath.Walk(".", func(fpath string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if path.Clean(info.Name()) == "." {
			return nil
		}

		if info.IsDir() {
			clog.Debugf("Skipped %s because its dir", info.Name())
			return filepath.SkipDir
		}

		if !strings.HasSuffix(info.Name(), ".ipk") {
			clog.Debugf("Skipped %s because its not ipk", info.Name())
			return nil
		}

		clog.Infof("Loading package %s...", info.Name())
		payload, err := os.ReadFile(fpath)
		if err != nil {
			clog.Errorf("Load package %s failed, error: %s", info.Name(), err)
			return nil
		}

		var control []byte
		{
			clog.Infof("Unpacking package %s...", info.Name())
			tgz, err := tar_gz.NewTarGZipReader(bytes.NewReader(payload))
			if err != nil {
				clog.Errorf("Unpack package %s failed, error: %s", info.Name(), err)
				return nil
			}
			defer tgz.Close()

			for {
				hdr, err := tgz.Next()
				switch err {
				case nil:
				default:
					return nil
				}

				if path.Clean(hdr.Name) == "control.tar.gz" {
					control = make([]byte, hdr.Size)
					break
				}
			}

			clog.Infof("Loading package %s control...", info.Name())
			_, err = io.ReadFull(tgz, control)
			if err != nil {
				clog.Errorf("Load package %s control failed, error: %s", info.Name(), err)
				return nil
			}
		}

		pkg := make(Package)
		{
			clog.Infof("Unpacking package %s control...", info.Name())
			tgz, err := tar_gz.NewTarGZipReader(bytes.NewReader(control))
			if err != nil {
				clog.Errorf("Unpack package %s control failed, error: %s", info.Name(), err)
				return nil
			}
			defer tgz.Close()

			var metadata []byte
			for {
				hdr, err := tgz.Next()
				switch err {
				case nil:
				default:
					return nil
				}

				if path.Clean(hdr.Name) == "control" {
					metadata = make([]byte, hdr.Size)
					break
				}
			}

			clog.Infof("Loading package %s meta...", info.Name())
			_, err = io.ReadFull(tgz, metadata)
			if err != nil {
				clog.Errorf("Load package %s meta failed, error: %s", info.Name(), err)
				return nil
			}

			clog.Infof("Unpacking package %s meta...", info.Name())
			metas := strings.Split(string(metadata), "\n")
			for _, meta := range metas {
				meta = strings.TrimSpace(meta)

				if meta == "" {
					continue
				}

				k, v, ok := strings.Cut(meta, ": ")
				if !ok {
					continue
				}

				switch k {
				case "Package", "Version", "Depends", "License", "Section", "Architecture", "Installed-Size", "Description":
					pkg[k] = v
				}
			}
		}

		pkg["Filename"] = info.Name()
		pkg["Size"] = fmt.Sprint(info.Size())
		pkg["SHA256sum"] = utils.SHA256(payload)

		packages = append(packages, pkg)
		clog.Successf("Unpacked package %s meta", info.Name())
		return nil
	})
	if err != nil {
		clog.Errorf("Load packages failed, error: %s", err)
		return
	}

	clog.Infof("Creating packages index...")
	buf := bytes.NewBuffer(nil)
	for _, pkg := range packages {
		keys := make([]string, 0, len(pkg))
		for k := range pkg {
			keys = append(keys, k)
		}

		sort.Slice(keys, func(i, j int) bool {
			priotity_i, priotity_j := priotitys[keys[i]], priotitys[keys[j]]
			if priotity_i == 0 {
				priotity_i = 50
			}

			if priotity_j == 0 {
				priotity_j = 50
			}

			return priotity_i < priotity_j
		})

		for _, k := range keys {
			buf.WriteString(fmt.Sprintf("%s: %s\n", k, pkg[k]))
		}
		buf.WriteRune('\n')
	}
	os.WriteFile("Packages", buf.Bytes(), Perm)
	clog.Successf("Output packages index to %s", "Packages")

	{
		gzbuf := bytes.NewBuffer(nil)
		gz := gzip.NewWriter(gzbuf)
		gz.Write(buf.Bytes())
		gz.Close()

		os.WriteFile("Packages.gz", gzbuf.Bytes(), Perm)
	}
	clog.Successf("Output packages index to %s", "Packages.gz")
}
